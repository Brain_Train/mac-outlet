class Cart {
   constructor() {
      this.items = [];
      this.totalAmount = 0;
      this.totalPrice = 0;
      this.cartElement = document.querySelector('.cart')
   }

   addToCart(data) {
      const itemInCart = this.items.find(item => item.id === data.id)
      if (itemInCart) {
         itemInCart.amount++;
      } else {
         this.items.push({
            id: data.id,
            name: data.name,
            imgUrl: data.imgUrl,
            amount: 1,
            price: data.price
         })
      }
      this.setTotalPriceAndAmount()
      this.renderBasket()
   }

   removeFromCart(data) {
      this.items = this.items.filter(item => item.id !== data.id)
      this.setTotalPriceAndAmount()
      this.renderBasket()
   }

   setTotalPriceAndAmount() {
      const newData = this.items.reduce((acc, item) => {
         return {
            totalAmount: acc.totalAmount + item.amount,
            totalPrice: acc.totalPrice + (item.amount * item.price)
         }
      }, {totalAmount: 0, totalPrice: 0})

      Object.assign(this, newData)
   }

   renderBasket() {
      const title = this.cartElement.querySelector('h1');
      title.innerText = `items in catrt: ${this.totalAmount} ptc.`

      const itemsContainer = this.cartElement.querySelector('.cart-Items')
      itemsContainer.innerHTML = ''

      const itemsAsElement = this.items.map(item => {
         const block = document.createElement('div')
         block.className = 'cart-items__item'
         block.innerHTML = `
            <p> <img src="${item.imgUrl}" alt=""> ${item.name} x ${item.amount} = ${item.amount * item.price}</p>
            <hr>
         `
         const removeFromCart = document.createElement('button')
         removeFromCart.innerText = 'x';
         removeFromCart.onclick = () => {
            this.removeFromCart(item)
         }
         block.querySelector('p').after(removeFromCart)
         return block;
      })

      itemsContainer.append(...itemsAsElement)
      

      this.cartElement.querySelector('.total').innerText = `Total items: ${this.totalAmount} - Total price: ${this.totalPrice}$`
   }
}

const cart = new Cart()

class FilterService {
   constructor() {
      this.price = this.initialPrise;
      this.category = [];
      this.color = [];
      this.os = [];
      this.storage = [];
   }

   get initialPrise() {
      const sortedArr = [...items].sort((a,b) => a.price - b.price);
      return [
         sortedArr[0].price, 
         sortedArr[sortedArr.length - 1].price
      ];
   }

   get initialColor() {
      return items.reduce((acc, item) => {
         item.color.forEach(colorInArr => {
            if (acc.includes(colorInArr)) return;
            acc.push(colorInArr)
         });
         return acc;
      }, [])
   }

   get initialOs() {
      return items.reduce((acc, item) => {
         if (item.os !== null && !acc.includes(item.os)) {
            acc.push(item.os)
         }
         return acc;
      }, [])
   }

   get initialStorage() {
      return items.reduce((acc, item) => {
         if (item.storage !== null && !acc.includes(item.storage)) {
            acc.push(item.storage)
         }
         return acc;
      }, []) 
   }

   get initialCategory() {
      return items.reduce((acc, item) => {
         if (item.category !== null && !acc.includes(item.category)) {
            acc.push(item.category)
         }
         return acc;
      }, [])
   }

   changeColor(color) {
      if (this.color.includes(color)) {
         this.color = this.color.filter(c => c !== color);
      } else {
         this.color.push(color)
      }
      this.filterAndRender()
   }

   changeOs(os) {
      if (this.os.includes(os)) {
         this.os = this.os.filter(o => o !== os);
      } else {
         this.os.push(os)
      }
      this.filterAndRender()
   }

   changePrice(value, type) {
      if (type === 'from') {
         this.price[0] = value;
      } else {
         this.price[1] = value;
      }
      this.filterAndRender()
   }

   changeStorage(storage) {
      if (this.storage.includes(this.storage)) {
         this.storage = this.storage.filter(s => s !== storage);
      } else {
         this.storage.push(storage)
      }
      this.filterAndRender()
   }

   changeCategory(category) {
      if (this.category.includes(this.category)) {
         this.category = this.category.filter(c => c !== category);
      } else {
         this.category.push(category)
      }
      this.filterAndRender()
   }

   filterArr() {
      return items.filter(item => {
         if (!(item.price >= this.price[0] 
            && item.price <= this.price[1])) return;

            // let isColor = false;
            // for (let color of this.color) {
            //    if (item.color.includes(color)){
            //       isColor = true;
            //       break;
            //    }
            // }
            let isColor = !this.color.length || this.color.some(c => item.color.includes(c));
            if (!isColor) return;

            let isOs = !this.os.length || this.os.includes(item.os);
            if (!isOs) return;

            let isStorage = !this.storage.length || this.storage.includes(item.storage);
            if (!isStorage) return;

            let isCategory = !this.category.length || this.category.includes(item.category);
            if (!isCategory) return;

            return true;
      })
   }

   filterAndRender(){
      const filtredArray = this.filterArr();
      render.renderCard(filtredArray);
   }
}

class FilterRenerOption {
   constructor(){
      this.filterService = new FilterService();
      this.options = [
         {
            title: 'Price',
            options: this.filterService.initialPrise,
            setter: this.filterService.changePrice.bind(this.filterService),
         },
         {
            title: 'Category',
            options: this.filterService.initialCategory,
            setter: this.filterService.changeCategory.bind(this.filterService),
         },
         {
            title: 'Color',
            options: this.filterService.initialColor,
            setter: this.filterService.changeColor.bind(this.filterService),
         },
         {
            title: 'OS',
            options: this.filterService.initialOs,
            setter: this.filterService.changeOs.bind(this.filterService),
         },
         {
            title: 'Storage',
            options: this.filterService.initialStorage,
            setter: this.filterService.changeStorage.bind(this.filterService),
         },
      ]
      this.renderFilters()
   }
   

   renderFilters(){
      const filtersContainer = document.querySelector('.filterOption');
      this.options.forEach(data => {
         const block = document.createElement('div')
         block.className = 'filterOption__block'

         const header = document.createElement('div')
         header.className = 'filterOption__header';
         header.innerHTML = `
            <strong>${data.title}</strong>
            <i class="fas fa-chevron-right"></i>
            `

         header.onclick = () => {
            const isActive = container.classList.toggle('filterOption__container--active')
            header.innerHTML = `
            <strong>${data.title}</strong>
            <i class="fas fa-chevron-${isActive ? 'down': 'right'}"></i>
            `
         }

         const container = document.createElement('div')
         container.className = 'filterOption__container';

         if (data.title === 'Price') {

            // from
            
            const lableFrom = document.createElement('label')
            lableFrom.innerText = 'From';
   
            const inputFrom = document.createElement('input')
            inputFrom.className = 'inputFtom'
            inputFrom.value = data.options[0]
            inputFrom.oninput = e => {
               data.setter(e.target.value, 'from')
               }
            lableFrom.appendChild(inputFrom)

               // to

            const lableTo = document.createElement('label')
            lableTo.innerText = 'To';
   
            const inputTo = document.createElement('input')
            inputTo.className = 'inputTo'
            inputTo.value = data.options[1]
            inputTo.oninput = e => {
               data.setter(e.target.value, 'to')
               }
            lableTo.appendChild(inputTo)

            container.append(lableFrom, lableTo)

         } else {
            data.options.forEach(option => {
               const lable = document.createElement('label')
               lable.innerText = option;
   
               const input = document.createElement('input')
               input.className = 'input'
               input.type = 'checkbox'
               input.onchange = e => {
                  data.setter(option)
               }
               lable.appendChild(input)
               container.appendChild(lable)
            })
         }


         block.append(header, container)
         filtersContainer.appendChild(block)


      })
   }
}

const filterRenderOption = new FilterRenerOption()


class BindEvents {
   constructor() {
      this.options = {
         search: '',
         sort: 'default',
      }
      this.searchInput = document.getElementById('searchImput')
      this.sortSelect = document.getElementById('sortSelect')
      this.bindEventsOnInput()
      this.bindEventsOnSort()
   }

   set sort(value) {
      this.options.sort = value;
      this.filterRender()
   }

   set search(value) {
      this.options.search = value;
      this.filterRender()
   }

   filterRender() {
      const {search, sort} = this.options;
      const filtredItems = items
         .filter(item => {
            return item.name.toLowerCase().includes(search.toLowerCase())
         })

      if (sort === 'default') {
         return this.renderCard(filtredItems);
      }

      filtredItems.sort((a, b) => {
         return sort === 'asc' ? a.price - b.price : b.price - a.price;
      })
      this.renderCard(filtredItems)
   }

   bindEventsOnInput() { 
      this.searchInput.oninput = (event) => {
         this.search = event.target.value
      }
   }

   bindEventsOnSort() { 
      this.sortSelect.onchange = (event) => {
         // console.log(event.target.value);
         this.sort = event.target.value
      }
      
   }
}

class Render extends BindEvents{ 
   constructor() {
      super();
      this.cardItem = document.querySelector('.blockCard');
      this.modal = document.querySelector('.modal');
      this.modalContainer = document.querySelector('.modal__modalContainer')
      this.bunnerButton = document.querySelector('.ipad_air_banner1')


      this.renderCard()


   }
   
   _renderCards(data){
      const card = document.createElement('div');
      card.onclick = (event) => {
         this.renderModal(data)
      }
      card.className = 'card';


      card.innerHTML = `
      <img class="cardImages" src="${data.imgUrl}" alt="">
      <h3 class="allLable">${data.name}</h3>
      <p class="infText">${data.orderInfo.inStock} left in stock</p>
      <p class="infText">Prise: ${data.price} $</p>
      `


      const button = document.createElement('button')
      button.className = 'bannerButton'
      button.innerHTML = 'Add to cart'
      button.onclick = (event) => {
         event.stopPropagation()
         cart.addToCart(data);
      }
      this.bunnerButton.appendChild(button)


      // cart.addToCart

      const addToCart = document.createElement('button');
      addToCart.className = 'addToCart';
      addToCart.innerHTML = 'Add to Cart';
      addToCart.onclick = (event) =>{
         event.stopPropagation()
         cart.addToCart(data);
      } 
      card.appendChild(addToCart);

      const footCrard = document.createElement('div');
      footCrard.className = 'footCard';
      card.appendChild(footCrard);

      footCrard.innerHTML = `
      <img src="../phone_store/img/icons/like_filled.svg" alt="">
      <p class="pshka">${data.orderInfo.reviews}% Positive reviews</p>
      <p class="pshka">${Math.floor((Math.random() * 900) + 100)} orders</p>
      `

      return card
   }


   renderCard(arr=items) {
      this.cardItem.innerHTML = ' ';
      const cards = arr.map(data => this._renderCards(data))
      this.cardItem.append(...cards);
   }
  

   renderModal(data) {
      this.modal.classList.add('active');
      this.modalContainer.innerHTML = `
      <div class="imgModal">
         <img src="${data.imgUrl}" alt="">
      </div>
      <div class="centralModal">
         <h2>${data.name}</h2>
         <div class="footCardInModal">
         <p class="pshka">${data.orderInfo.reviews}% Positive reviews</p>
         <p class="pshka">${Math.floor((Math.random() * 900) + 100)} orders</p>
         </div>
         <p class="infText">Display: ${data.display} em</p>
         <p class="infText">Chip: ${data.chip.name}</p>
         <p class="infText">Cores: ${data.chip.cores}</p>
         <p class="infText">Storage: ${data.storage} gb</p>
         <p class="infText">Height: ${data.size.height} cm</p>
         <p class="infText">Width: ${data.size.width} cm</p>
         <p class="infText">Depth: ${data.size.depth} cm</p>
         <p class="infText">Weight: ${data.size.weight} kg</p>
      </div>
      `
      const divForButton = document.createElement('div')
      divForButton.className = 'forButton'
      this.modalContainer.append(divForButton)

      divForButton.innerHTML = `
         <h3>$ ${data.price}</h3>
         <h4> ${data.orderInfo.inStock} left in stock </h4>
      `
      const button = document.createElement('button');
      button.className = 'addToCart';
      button.innerText = 'Add to Cart';
      button.onclick = (e) => {
         cart.addToCart(data)
      }
      divForButton.appendChild(button)
      
      this.modalContainer.onclick = (e) => e.stopPropagation();
      this.modal.onclick = () => {
         // if (this.modal !== event.target) return;     // просто убрать модалконтейнер он клик и поставить это и будет то же самое
         this.modal.classList.remove('active');
      }
   }
}
      
const render = new Render()




document.addEventListener('keydown', (e) => {
   if (e.key === 'Escape') {
      render.modal.classList.remove('active');
   }
   // else if (e.key === 'Enter') {
   //    render.modal.classList.add('active')
   // }
})


// patern single ton ( i need read something about this ) fabrica maybe (i can read this but its nit a nessecery) 
// refactoring guru 